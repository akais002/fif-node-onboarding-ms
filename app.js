const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const config = require('./app/config/');

const Route = require('./app/routes/token-route'); // eslint-disable-line

const app = express();

app.set('trust proxy', true);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression());

// health check MS
app.get('/api/user/token/health/', (req, res) => {
  res.send(`${config.msConfig.name} up and running`);
});

app.use(`/api/${config.context.version}/user/token`, Route);

module.exports = app;
