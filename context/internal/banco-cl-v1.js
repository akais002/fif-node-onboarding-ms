module.exports = {
  middlewares: [
    'check-version-middleware',
    'check-signature-middleware',
    'validate-schema-middleware'
  ],
  country: 'cl',
  commerce: 'banco',
  channel: 'web',
  version: 'v1'
};
