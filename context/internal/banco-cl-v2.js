module.exports = {
  middlewares: [
    'check-version-middleware',
    'check-signature-middleware',
    'check-country-middleware',
    'validate-body-middleware',
    'get-token-middleware'
  ],
  country: 'cl',
  commerce: 'banco',
  channel: 'web',
  version: 'v2'
};
