const fs = require('fs');

const envApp = process.env.NODE_ENV_APP; // || 'banco-cl';
const contextPath = `${__dirname}/internal/${envApp}.js`;
if (!fs.existsSync(contextPath)) {
  throw new Error(`the context file ${contextPath} was not found, set correctly in the context folder`);
}
const contextConfiguration = require(contextPath); // eslint-disable-line
module.exports = Object.freeze(contextConfiguration);
