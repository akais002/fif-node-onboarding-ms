const GetTokenController = require('../controllers/get-token-controller');

async function getTokenMiddleware(req, res) {
  console.debug('middleware getTokenMiddleware');
  try {
    const getTokenController = new GetTokenController();
    const response = await getTokenController.getToken(req.body);
    return res.status(200).send({ code: 'ok', message: response });
  } catch (e) {
    console.error(e.message);
    return res.status(500).send({ code: 'error', message: e.message });
  }
}

module.exports.getTokenMiddleware = getTokenMiddleware;
