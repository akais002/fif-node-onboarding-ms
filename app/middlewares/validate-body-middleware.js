const setResponseWithError = require('../util/common-response').setResponseWithError;

module.exports.validateBodyMiddleware = (req, res, next) => {
  console.debug('middleware validateBodyMiddleware');
  if (req.body.id && req.body.id.length > 6) {
    return next();
  }
  return setResponseWithError(res, 400, 'the body is not correct  ');
};
