const setResponseWithError = require('../util/common-response').setResponseWithError;
const config = require('../config');

module.exports.checkVersionMiddleware = (req, res, next) => {
  console.debug('middleware checkVersionMiddleware');
  if (req.headers['accept-version'] === config.context.version) {
    return next();
  }
  return setResponseWithError(res, 400, 'the version is not correct');
};
