const setResponseWithError = require('../util/common-response').setResponseWithError;
const config = require('../config');

module.exports.checkCommerceMiddleware = (req, res, next) => {
  console.debug('middleware checkCommerceMiddleware');
  if (req.headers['x-flow-commerce'] === config.context.commerce) {
    return next();
  }
  return setResponseWithError(res, 400, 'the commerce is not correct');
};
