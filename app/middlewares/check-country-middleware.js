const setResponseWithError = require('../util/common-response').setResponseWithError;
const config = require('../config');

module.exports.checkCountryMiddleware = (req, res, next) => {
  console.debug('middleware checkCountryMiddleware');
  if (req.headers['x-flow-country'] === config.context.country) {
    return next();
  }
  return setResponseWithError(res, 400, 'the country is not correct');
};
