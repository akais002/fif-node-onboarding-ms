const setResponseWithError = require('../util/common-response').setResponseWithError;
const config = require('../config');

module.exports.checkChannelMiddleware = (req, res, next) => {
  console.debug('middleware checkChannelMiddleware');
  if (req.headers['x-flow-channel'] === config.context.channel) {
    return next();
  }
  return setResponseWithError(res, 400, 'the channel is not correct');
};
