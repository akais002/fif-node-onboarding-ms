const rp = require('request-promise');
const config = require('../config');

module.exports = function () {
  this.getToken = async (body) => {
    try {
      const response = await this.restClient(body, this.headers());
      console.debug('the response', response);
      return this.createResponse(body, response);
    } catch (e) {
      console.error(e.message);
      throw e;
    }
  };

  this.createResponse = (request, response) => {
    return {
      id: request.id,
      token: response.message.token
    };
  };

  this.restClient = (body, headers) => {
    const options = {
      method: 'POST',
      uri: config.msConfig.tokenServiceConfig.uri,
      body,
      timeout: config.msConfig.tokenServiceConfig.timeout,
      headers,
      json: true
    };
    return rp(options);
  };

  this.headers = () => {
    return {
      'content-type': 'application/json',
      'accept-version': 'v1'
    };
  };
};
