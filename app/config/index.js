const fs = require('fs');
const contextConfig = require('../../context');
// const localLogger = require('../local-logger/local-logger');

const commonConfig = {};
if (Object.keys(commonConfig).length === 0) {
  commonConfig.context = contextConfig;
  const env = process.env.NODE_ENV; // || 'local';
  const envApp = process.env.NODE_ENV_APP; // || 'banco-cl';
  if (!fs.existsSync(`${__dirname}/config.${env}`)) {
    // localLogger.error('the NODE_ENV was not found');
    throw new Error(`the config file ${__dirname}/config.${env} was not found, set correctly the env variable NODE_ENV`);
  }
  const cfg = require(`./config.${env}`); // eslint-disable-line
  console.info(cfg);
  commonConfig.msConfig = cfg;
  commonConfig.env = env;
  commonConfig.envApp = envApp;
  // localLogger.info(' getting the json config %s', JSON.stringify(commonConfig));
}

module.exports = Object.freeze(commonConfig);
