var mockery = require('mockery');
const chai    = require('chai');
const sinon = require('sinon');
const expect = chai.expect;
describe('index config', () => {

  beforeEach(function () {
    mockery.enable({
        warnOnReplace: false,
        warnOnUnregistered: false,
        useCleanCache: true
    });
  });

  afterEach(function () {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('index', () => {

    it('should throw an exception when then config folder does not exist', function () {
      const fsMock = {
        existsSync: (url)  => {
          if(process.env.NODE_ENV_APP==='no-folder'){
            return false;
          }else{
            return true;
          }
        }
      }
      mockery.registerMock('fs', fsMock);
      process.env.NODE_ENV_APP= 'no-folder';      
      expect(() =>{const config = require('./index');}).to.throw(Error);
    });

    it('should throw an exception when then config file does not exist', function () {
      const fsMock = {
        existsSync: (url)  => {
          if(url.indexOf('other_config') >= 0){
            return false;
          }else{
            return true;
          }
        }
      }
      mockery.registerMock('fs', fsMock);
      process.env.NODE_ENV_APP= 'default';
      process.env.NODE_ENV= 'other_config';      
      expect(() =>{const config = require('./index');}).to.throw(Error);
    });

    it('should return the local configuration', function () {
      const fsMock = {
        existsSync: (url)  => {
            return true;
        }
      }
      mockery.registerMock('fs', fsMock);
      process.env.NODE_ENV_APP= 'banco-cl';
      process.env.NODE_ENV= 'local';
      const config = require('./index');
      expect(config).to.be.an('object')
    });

  });

});
