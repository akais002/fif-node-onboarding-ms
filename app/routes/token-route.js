const Router = require('express').Router;
const config = require('../config/');
const getMiddlewares = require('../util/get-middleware').getMiddlewares;

console.debug('token route');
const router = Router();
router.post('/', getMiddlewares(config.context.middlewares));

module.exports = router;
