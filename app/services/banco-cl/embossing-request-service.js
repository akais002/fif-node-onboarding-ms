const SoapWrapper = require('fif-soap-client-wrapper');
const path = require('path');
const config = require('../../config/index');

const EmbossingRequestService = function () {
  const self = this;
  this.callBackend = (schema) => {
    return new Promise((resolve, reject) => {
      try {
        const soapService = new SoapWrapper(
          path.join(__dirname,
            config.backendConfig.PlasticoSolicitudGenerar.PlasticoSolicitudGenerarOpWsdl),
          config.backendConfig.PlasticoSolicitudGenerar.clienteIdentidadExternaValidarUrl,
          config.backendConfig.PlasticoSolicitudGenerar.operation,
          config.backendConfig.PlasticoSolicitudGenerar.timeOut
        );
        soapService.invoke(
          self.getSoapHeaderParams(),
          self.getSoapRequestParams(schema),
          (error, result) => {
            if (result) {
              resolve(result);
            } else {
              reject(error);
            }
          });
      } catch (internalError) {
        reject(internalError.message);
      }
    });
  };

  this.getSoapRequestParams = (schema) => {
    return {
      documentoIdentidad: {
        tipoDocumento: schema.tipoDocumento,
        numeroDocumento: schema.numeroDocumento
      },
      producto: {
        codigoProducto: schema.codigoProducto
      },
      direccion: {
        codigoTipoDireccion: schema.codigoTipoDireccion
      },
      plastico: {
        tipoPlastico: schema.tipoPlastico,
        situacion: {
          codigo: schema.codigoSituacion
        }
      },
      lineaCredito: {
        identificadorProducto: schema.identificadorProducto,
        cuentaCorrienteCargo: schema.cuentaCorrienteCargo,
        cupo: {
          cupoTotal: schema.cupoTotal
        }
      },
      sucursal: {
        codigoEmpleado: schema.codigoEmpleado,
        codigoSucursal: schema.codigoSucursal
      }
    };
  };

  this.getSoapHeaderParams = () => {
    const headerClientService = {
      json: { ClientService: config.backendConfig.headerConfig.clientService.headers },
      nsprefix: config.backendConfig.headerConfig.clientService.nsprefix,
      namespaceUri: config.backendConfig.headerConfig.clientService.namespaceUri
    };
    const headerClientServiceFIF = {
      json: { ClientServiceFIF: config.backendConfig.headerConfig.clientServiceFIF.headers },
      nsprefix: config.backendConfig.headerConfig.clientServiceFIF.nsprefix,
      namespaceUri: config.backendConfig.headerConfig.clientServiceFIF.namespaceUri
    };
    const headers = [headerClientServiceFIF, headerClientService];
    return headers;
  };
};

module.exports = EmbossingRequestService;
