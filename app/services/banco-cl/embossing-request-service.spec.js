
const mockery = require('mockery');
const chai = require('chai');
const sinon = require('sinon');

const expect = chai.expect;

describe('EmbossingRequestService', () => {
    beforeEach(function () {
        mockery.enable({
            warnOnReplace: false,
            warnOnUnregistered: false,
            useCleanCache: true
        });
        mockery.registerMock('../../loggers/logger', {
          logger: {
              debug: function () { },
              info: function () { },
              error: function () { },
              funName: function () { }              
          
            }
          });
        mockery.registerMock('fif-soap-client-wrapper',{});    
        mockery.registerMock('../../config/index',{});    
        mockery.registerMock('path',{ join: ()=>{}});
    });

    afterEach(function () {
        mockery.disable();
        mockery.deregisterAll();
    });

    it("Should be an instance of EmbossingRequestService", () => {        
        const Service = require('./embossing-request-service');
        const service = new Service();

        expect(service instanceof Service).to.be.true;
    });

    describe("getSoapRequestParams method", () => {

      it("should return ok from the promise", (done) => {       
        mockery.registerMock('../../config/index',{
          backendConfig:{
            PlasticoSolicitudGenerar: {
              PlasticoSolicitudGenerarOpWsdl: '',
              clienteIdentidadExternaValidarUrl: '',
              operation: '',
              timeOut: '',
            }
          }
        });   
        let Mock = function (path, url, operation, timeOut) {
          this.invoke = (headers, request, cb) =>{
             cb(null, 'ok');
          }
        }
        mockery.registerMock('fif-soap-client-wrapper', Mock);

        const Service = require('./embossing-request-service');
        const service = new Service();          
        sinon.stub(service, 'getSoapHeaderParams').returns({});   
        sinon.stub(service, 'getSoapRequestParams').returns({});   
        service.callBackend({}).then((response) =>{
          expect(response).to.be.equal('ok');
          done();
        });       
      });

      it("should return an error when the promise was rejected", (done) => {       
        mockery.registerMock('../../config/index',{
          backendConfig:{
            PlasticoSolicitudGenerar: {
              PlasticoSolicitudGenerarOpWsdl: '',
              clienteIdentidadExternaValidarUrl: '',
              operation: '',
              timeOut: '',
            }
          }
        });   
        let Mock = function (path, url, operation, timeOut) {
          this.invoke = (headers, request, cb) =>{
             cb('error', null);
          }
        }
        mockery.registerMock('fif-soap-client-wrapper', Mock);

        const Service = require('./embossing-request-service');
        const service = new Service();          
        sinon.stub(service, 'getSoapHeaderParams').returns({});   
        sinon.stub(service, 'getSoapRequestParams').returns({});   
        service.callBackend({}).catch((error) =>{
          expect(error).to.be.equal('error');
          done();
        });       
      });

      it("should return an error when  an internal error happens", (done) => {       
        mockery.registerMock('../../config/index',{
          backendConfig:{
            PlasticoSolicitudGenerar: {
              PlasticoSolicitudGenerarOpWsdl: '',
              clienteIdentidadExternaValidarUrl: '',
              operation: '',
              timeOut: ''
            }
          }
        })
        mockery.registerMock('fif-soap-client-wrapper', {});

        const Service = require('./embossing-request-service');
        const service = new Service();          
        sinon.stub(service, 'getSoapHeaderParams').returns({});   
        sinon.stub(service, 'getSoapRequestParams').returns({});   
        service.callBackend({}).catch((error) =>{
          expect(error).to.be.equal('SoapWrapper is not a constructor');
          done();
        });       
      });

    });


    describe("getSoapRequestParams method", () => {

      it("should return the first headers", () => {       

        const Service = require('./embossing-request-service');
        const service = new Service();          
        const schema = {
          "tipoDocumento": "RUT",
          "numeroDocumento": "14.216.541-4",
          "codigoProducto": "1",
          "codigoSituacion": "526",
          "codigoTipoDireccion": "1",                
          "tipoPlastico": "1",
          "identificadorProducto": "13730026184",
          "cuentaCorrienteCargo": "23730026036",
          "cupoTotal": "250000",
          "codigoEmpleado": "5210",
          "codigoSucursal": "588",                
        };
        const body = service.getSoapRequestParams(schema);        
        expect(body.documentoIdentidad.tipoDocumento).to.be.equal('RUT');
        expect(body.documentoIdentidad.numeroDocumento).to.be.equal('14.216.541-4');

        expect(body.producto.codigoProducto).to.be.equal('1');

        expect(body.direccion.codigoTipoDireccion).to.be.equal('1');

        expect(body.plastico.tipoPlastico).to.be.equal('1');
        expect(body.plastico.situacion.codigo).to.be.equal('526');

        expect(body.lineaCredito.identificadorProducto).to.be.equal('13730026184');
        expect(body.lineaCredito.cuentaCorrienteCargo).to.be.equal('23730026036');
        expect(body.lineaCredito.cupo.cupoTotal).to.be.equal('250000');

        expect(body.sucursal.codigoEmpleado).to.be.equal('5210');
        expect(body.sucursal.codigoSucursal).to.be.equal('588');        
      });

    });

    describe("getSoapHeaderParams method", () => {

      it("should return the first headers", () => {
        mockery.registerMock('../../config/index',{
          backendConfig:{
            headerConfig:{
              clientService:{
                headers:'headers1',
                nsprefix: 'nsprefix1',
                namespaceUri: 'namespaceUri1'
              },
              clientServiceFIF:{
                headers:'headers2',
                nsprefix: 'nsprefix2',
                namespaceUri: 'namespaceUri2'
              }
            }
          }
        });    

        const Service = require('./embossing-request-service');
        const service = new Service();          
        const headers = service.getSoapHeaderParams();
        const header = headers[0];        
        expect(header.json.ClientServiceFIF).to.be.equal('headers2');
        expect(header.nsprefix).to.be.equal('nsprefix2');
        expect(header.namespaceUri).to.be.equal('namespaceUri2');
      });

      it("should return the second headers", () => {
        mockery.registerMock('../../config/index',{
          backendConfig:{
            headerConfig:{
              clientService:{
                headers:'headers1',
                nsprefix: 'nsprefix1',
                namespaceUri: 'namespaceUri1'
              },
              clientServiceFIF:{
                headers:'headers2',
                nsprefix: 'nsprefix2',
                namespaceUri: 'namespaceUri2'
              }
            }
          }
        });    

        const Service = require('./embossing-request-service');
        const service = new Service();          
        const headers = service.getSoapHeaderParams();        
        const header = headers[1];                                           
        expect(header.json.ClientService).to.be.equal('headers1');
        expect(header.nsprefix).to.be.equal('nsprefix1');
        expect(header.namespaceUri).to.be.equal('namespaceUri1');
      });

    });

});
